package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Employee;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

// TODO: Auto-generated Javadoc
/**
 * The Class EmployeeController.
 */
@RestController
@Api(value="Employee Service",description="Entails the Employee Managaement Service")
public class EmployeeController {

	/** The employees. */
	static List<Employee> employees = new ArrayList<>();
	static {

		Employee e1 = new Employee("101", "Samik", "Roy", "2000");
		Employee e2 = new Employee("102", "Rahul", "Saha", "5000");
		Employee e3 = new Employee("103", "Tanmoy", "Pal", "7000");
		employees.add(e1);
		employees.add(e2);
		employees.add(e3);

	}

	/**
	 * Gets the employees.
	 *
	 * @return the employees
	 */
	@RequestMapping(value = "/employees", method = RequestMethod.GET)
	@ApiOperation(value = "List all the employee details")
	@ApiResponses(value={
			@ApiResponse(code=100,message="100 is the message"),
			@ApiResponse(code=200,message="200 is the message"),
			@ApiResponse(code=300,message="300 is the message"),
	})
	public @ResponseBody List<Employee> getEmployees() {

		// model.addAttribute("name","America is Great!!!!!!!!!!!!!!!!!");
		return employees;

	}

	/**
	 * Gets the employee by id.
	 *
	 * @param id
	 *            the id
	 * @return the employee by id
	 */
	@RequestMapping(value = "/getemployee/{id}", method = RequestMethod.GET)
	@ApiOperation(value = "List a specific employee detail by id")
	@ApiResponses(value={
			@ApiResponse(code=100,message="100 is the message"),
			@ApiResponse(code=200,message="200 is the message"),
			@ApiResponse(code=300,message="300 is the message"),
	})
	public @ResponseBody Employee getEmployeeById(@PathVariable("id") int id) {

		// model.addAttribute("name","America is Great!!!!!!!!!!!!!!!!!");
		return employees.get(id);

	}

	/**
	 * Delete employee.
	 *
	 * @param id
	 *            the id
	 * @return the employee
	 */
	@RequestMapping(value = "/deleteemployee/{id}", method = RequestMethod.DELETE)
	@ApiOperation(value = "Delete a specific employee detail by id")
	@ApiResponses(value={
			@ApiResponse(code=100,message="100 is the message"),
			@ApiResponse(code=200,message="200 is the message"),
			@ApiResponse(code=300,message="300 is the message"),
	})
	public @ResponseBody Employee deleteEmployee(@PathVariable("id") int id) {

		// model.addAttribute("name","America is Great!!!!!!!!!!!!!!!!!");
		return employees.remove(id);

	}

	/**
	 * Delete employee.
	 *
	 * @param id
	 *            the id
	 * @param employee
	 *            the employee
	 * @return the employee
	 */
	@RequestMapping(value = "/updateemployee/{id}", method = RequestMethod.PUT, consumes = "application/json")
	@ApiOperation(value = "Update a specific employee detail by id")
	@ApiResponses(value={
			@ApiResponse(code=100,message="100 is the message"),
			@ApiResponse(code=200,message="200 is the message"),
			@ApiResponse(code=300,message="300 is the message"),
	})
	public @ResponseBody Employee updateEmployee(@PathVariable("id") int id, @RequestBody Employee employee) {

		// model.addAttribute("name","America is Great!!!!!!!!!!!!!!!!!");
		return employees.set(id, employee);

	}

	/**
	 * Insert employee.
	 *
	 * @param employee
	 *            the employee
	 */
	@RequestMapping(value = "/insertemployee", method = RequestMethod.POST, consumes = "application/json")
	@ApiOperation(value = "Insert a specific employee detail")
	@ApiResponses(value={
			@ApiResponse(code=100,message="100 is the message"),
			@ApiResponse(code=200,message="200 is the message"),
			@ApiResponse(code=300,message="300 is the message"),
	})
	public @ResponseBody void insertEmployee(@RequestBody Employee employee) {

		// model.addAttribute("name","America is Great!!!!!!!!!!!!!!!!!");
		employees.add(employee);

	}

}
