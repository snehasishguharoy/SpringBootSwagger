package com.example.demo.model;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;

// TODO: Auto-generated Javadoc
/**
 * The Class Employee.
 */
public class Employee implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 56979794136798244L;
	
	/** The emp id. */
	@ApiModelProperty(notes="id of the employee")
	private String empId;
	
	/** The first name. */
	@ApiModelProperty(notes="first name of the employee")
	private String firstName;
	
	/** The last name. */
	@ApiModelProperty(notes="last name of the employee")
	private String lastName;
	
	/** The salary. */
	@ApiModelProperty(notes="salary of the employee")
	private String salary;

	/**
	 * Gets the emp id.
	 *
	 * @return the emp id
	 */
	public String getEmpId() {
		return empId;
	}

	/**
	 * Sets the emp id.
	 *
	 * @param empId the new emp id
	 */
	public void setEmpId(String empId) {
		this.empId = empId;
	}

	/**
	 * Gets the first name.
	 *
	 * @return the first name
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Sets the first name.
	 *
	 * @param firstName the new first name
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Instantiates a new employee.
	 */
	public Employee() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Instantiates a new employee.
	 *
	 * @param empId the emp id
	 * @param firstName the first name
	 * @param lastName the last name
	 * @param salary the salary
	 */
	public Employee(String empId, String firstName, String lastName, String salary) {
		super();
		this.empId = empId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.salary = salary;
	}

	/**
	 * Gets the last name.
	 *
	 * @return the last name
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Sets the last name.
	 *
	 * @param lastName the new last name
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Gets the salary.
	 *
	 * @return the salary
	 */
	public String getSalary() {
		return salary;
	}

	/**
	 * Sets the salary.
	 *
	 * @param salary the new salary
	 */
	public void setSalary(String salary) {
		this.salary = salary;
	}

}
